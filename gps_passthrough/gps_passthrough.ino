#include <Wire.h>
#include <Sodaq_wdt.h>

#define GPS_STREAM Wire
#define GPS_ADR 0x42

#define CONSOLE_STREAM SerialUSB
#define CONSOLE_BAUD 115200

#define READ_TIMEOUT_MS 100

#define BUFFER_SIZE 256
uint8_t buffer[BUFFER_SIZE];

uint32_t start;

#define GALILEO       // enables Galileo navigation
#define NMEA41        // use NMEA version 4.1
#define RXMSFRBX      // output UBX RXM-SFRBX messages

// ONLY USE ONE OF THE MESSAGES BELOW AT ONCE
//#define GLL       // geographic position, latitude, longitude
#define GGA         // global positioning system fix data
//#define GSA       // gps dop and active satellites
//#define GSV       // gps satellites in view
//#define RMC       // recommended minimum specific gps/transit data
//#define VTG       // track made good and ground speed

const byte gnnsConf1[] = {0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x07, 0x0C, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, 0x01, 0x02, 0x06, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x07, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x29, 0x0D};
const byte gnnsConf2[] = {0xB5, 0x62, 0x06, 0x3E, 0x00, 0x00, 0x44, 0xD2};
const byte gnnsConf3[] = {0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x20, 0x20, 0x07, 0x00, 0x07, 0x0C, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, 0x01, 0x02, 0x06, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x03, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x05, 0x06, 0x07, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x4F, 0xB3};

const byte navPosllh[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x02, 0x01, 0x0E, 0x47};
const byte navTimegal[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x25, 0x01, 0x31, 0x8D};
const byte navSat[] =     {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x35, 0x01, 0x41, 0xAD};
const byte rxmsfrbx[] =   {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x02, 0x13, 0x01, 0x20, 0x6C};

// disable all NMEA messages
const byte disableGGA[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x00, 0x00, 0xFA, 0x0F};
const byte disableGLL[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x01, 0x00, 0xFB, 0x11};
const byte disableGSA[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x02, 0x00, 0xFC, 0x13};
const byte disableGSV[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
const byte disableRMC[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x04, 0x00, 0xFE, 0x17};
const byte disableVTG[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x05, 0x00, 0xFF, 0x19};

void setup()
{
    delay(3000); // give serial time to get ready
    gpsEnable(true);
    Wire.begin();

    CONSOLE_STREAM.begin(CONSOLE_BAUD);
    delay(200);

    sendGNSSConfig();
    delay(200);
    start = millis();
}

void loop()
{  
    // uncomment to use with u-center
    //Console -> GPS
    memset(buffer, 0, BUFFER_SIZE);
    writeGPS(readConsole());

    //GPS -> Console
    memset(buffer, 0, BUFFER_SIZE);
    writeConsole(readGPS());
/*
    // uncomment to handle the messages received by the receiver
    memset(buffer, 0, BUFFER_SIZE);
    size_t count = readGPS();
    if (count != 0) {
        String data = (char*) buffer;
        CONSOLE_STREAM.println(data);
        if (data.startsWith("$GNGGA")) {
            // handle GGA
        }
        else if (data.startsWith("$GNTXT")) {
            // ignore GNTXT, no useful data
            //CONSOLE_STREAM.println("$GATXT received, ignoring");
        }
        else {
            // handle RXM-SFRBX
            parseUBX_RXM_SFRBX(count);
        }
    }
*/
}

void parseUBX_RXM_SFRBX(size_t count) {
    byte *p = buffer + 6; // payload start
    uint8_t gnssId, svId, numWords;
    uint32_t w1, w2, w3, w4, w5, w6, w7, w8;
    // U1 = unsigned char
    // U4 = unsigned long
    
    // offset   format  name        description
    // 0        U1      gnssId      GNSS identifier
    // 1        U1      svId        satellite identifier
    // 4        U1      numWords    number of data words, up to 10
    // 8 + 4*N  U4      dwrd        the data words (N = numWords)
    gnssId      = *((uint8_t *) &p[0x00]);
    // only care about frames from Galileo
    if (gnssId == 2) {
        svId        = *((uint8_t *) &p[0x01]);
        numWords    = *((uint8_t *) &p[0x04]);  
        w1          = *((uint32_t *) &p[0x08]);
        w2          = *((uint32_t *) &p[0x0C]);
        w3          = *((uint32_t *) &p[0x10]);
        w4          = *((uint32_t *) &p[0x14]);
        w5          = *((uint32_t *) &p[0x18]);
        w6          = *((uint32_t *) &p[0x1C]);
        w7          = *((uint32_t *) &p[0x20]);    
        w8          = *((uint32_t *) &p[0x24]);
        CONSOLE_STREAM.println("RXM-SFRBX "); 
        CONSOLE_STREAM.print("GNSS ID: ");
        CONSOLE_STREAM.println(gnssId); 
        CONSOLE_STREAM.print("Satellite ID: ");
        CONSOLE_STREAM.println(svId); 
        CONSOLE_STREAM.print("NumWords: ");
        CONSOLE_STREAM.println(numWords);
        if (numWords <= 10) {
            CONSOLE_STREAM.println("Data words: ");
            CONSOLE_STREAM.print("DW1: ");
            CONSOLE_STREAM.println(w1, BIN);
            CONSOLE_STREAM.print("DW2: ");
            CONSOLE_STREAM.println(w2, BIN);
            CONSOLE_STREAM.print("DW3: ");
            CONSOLE_STREAM.println(w3, BIN);
            CONSOLE_STREAM.print("DW4: ");
            CONSOLE_STREAM.println(w4, BIN);
            CONSOLE_STREAM.print("DW5: ");
            CONSOLE_STREAM.println(w5, BIN);
            CONSOLE_STREAM.print("DW6: ");
            CONSOLE_STREAM.println(w6, BIN);
            CONSOLE_STREAM.print("DW7: ");
            CONSOLE_STREAM.println(w7, BIN);
            CONSOLE_STREAM.print("DW8: ");
            CONSOLE_STREAM.println(w8, BIN);
        }
        CONSOLE_STREAM.println(String("time: ") + (millis()-start) + String("ms"));
        start = millis();
        CONSOLE_STREAM.println("-----------------------------------------");
    }   
}

void gpsEnable(bool state)
{
    //Enable GPS module
    pinMode(GPS_ENABLE, OUTPUT);
    digitalWrite(GPS_ENABLE, state);
}

size_t readGPS()
{
    return readUbloxI2cStream();
}

size_t readConsole()
{
    return readSerialStream((Stream*)&CONSOLE_STREAM);
}

size_t readUbloxI2cStream()
{
    uint16_t count = 0;
    Wire.beginTransmission(GPS_ADR);
    Wire.write((uint8_t)0xFD);
    Wire.endTransmission(false);
    Wire.requestFrom(GPS_ADR, 2);
    count = (uint16_t)(Wire.read() << 8) | Wire.read();
    count = (count > BUFFER_SIZE) ? BUFFER_SIZE : count;

    if (count) {
        for (size_t i = 0; i < (count - 1); i++) {
            Wire.requestFrom(GPS_ADR, 1, false);
            buffer[i] = Wire.read();
        }
        Wire.requestFrom(GPS_ADR, 1);
        buffer[count - 1] = Wire.read();
    }

    return count;
}

size_t readSerialStream(Stream* stream)
{
    uint32_t last = millis();
    size_t count = 0;
    while ((count < BUFFER_SIZE) && (millis() < (last + READ_TIMEOUT_MS))) {
        if (stream->available()) {
            buffer[count++] = stream->read();
            last = millis();
        }
    }
    return count;
}

void writeGPS(size_t count)
{
    Wire.beginTransmission(GPS_ADR);
    Wire.write(buffer, count);
    Wire.endTransmission();
}

void writeConsole(size_t count)
{
    for (size_t i = 0; i < count; i++) {
        CONSOLE_STREAM.write(buffer[i]);
    }
}

void sendGNSSConfig()
{   
    /*
    CONSOLE_STREAM.println(sizeof(nmea3));
    CONSOLE_STREAM.println(sizeof(nmea4));
    CONSOLE_STREAM.println(sizeof(nmea7));
    CONSOLE_STREAM.println(sizeof(nmea9));
    CONSOLE_STREAM.println(sizeof(nmea10));
    CONSOLE_STREAM.println(sizeof(nmea12));
    */
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf1[0], sizeof(gnnsConf1));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf2[0], sizeof(gnnsConf2));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf3[0], sizeof(gnnsConf3));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGLL[0], sizeof(disableGLL));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSA[0], sizeof(disableGSA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSV[0], sizeof(disableGSV));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableRMC[0], sizeof(disableRMC));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
     
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableVTG[0], sizeof(disableVTG));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    // for some reason needs to be done twice..
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navPosllh[0], sizeof(navPosllh));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5); // short delay needed between transmissions
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navTimegal[0], sizeof(navTimegal));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navSat[0], sizeof(navSat));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&rxmsfrbx[0], sizeof(rxmsfrbx));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    /*
#ifdef NMEA41
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&changeNMEAVersion[0], sizeof(changeNMEAVersion));
    Wire.endTransmission();

    delay[5];   // short delay needed between transmissions
#endif    
#ifdef GALILEO
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&enableGalileo[0], sizeof(enableGalileo));
    Wire.endTransmission();

    delay(5);
#endif
*/
/*
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGLL[0], sizeof(disableGLL));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSA[0], sizeof(disableGSA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSV[0], sizeof(disableGSV));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableRMC[0], sizeof(disableRMC));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableVTG[0], sizeof(disableVTG));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(10);
    */
    /*
#ifdef RXMSFRBX
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&enableRXMSFRBX[0], sizeof(enableRXMSFRBX));
    Wire.endTransmission();

    delay[5];
#endif
*/

}
