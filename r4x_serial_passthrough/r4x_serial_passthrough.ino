#include <Sodaq_R4X.h>
#include <Arduino.h>
/* SODAQ SARA SFF */
#define DEBUG_STREAM SerialUSB
#define MODEM_STREAM Serial1
#define powerPin SARA_ENABLE
#define voltagePin SARA_R4XX_TOGGLE

static bool isReady;

unsigned long baud = 115200;  //start at 115200 allow the USB port to change the Baudrate

void setup() 
{
    pinMode(powerPin, OUTPUT);
    digitalWrite(powerPin, HIGH);
    
    pinMode(voltagePin, OUTPUT);
    digitalWrite(voltagePin, LOW);

    // Start communication
    DEBUG_STREAM.begin(baud);
    MODEM_STREAM.begin(baud);
}

// Forward every message to the other serial
void loop() 
{
  while (DEBUG_STREAM.available())
  {
    MODEM_STREAM.write(DEBUG_STREAM.read());
  }

  while (MODEM_STREAM.available())
  {     
    DEBUG_STREAM.write(MODEM_STREAM.read());
  }
}
