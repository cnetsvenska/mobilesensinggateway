/*
SAAM - Sound and air measurement

Version: 3.3

Last modified: 2019-11-21

Original author: Mikael Mölder

Very simplified program description:
1. Configures the GNSS modem to use the correct constellations and filter 
the NMEA me2ssage output (only for boards using the UBX M8M)

2. Connect to NB-IoT network

3. Obtain location fix of the device

4. Sample sound and air sensor for values

5. Send values via UDP to backend receiver

6. Repeat from step 3.
*/

#include <Sodaq_R4X.h>      // NB-IoT modem library
#include <Sodaq_wdt.h>      // watchdog timer library
#include <Wire.h>           // for communication with the GNSS module

/*********************************************
    Serial definitions
*********************************************/
//#define DEBUG   SerialUSB    // to pc, uncomment if not debugging
#define UBLOX   Serial1      // to nb-iot modem
#define SENSOR  Serial       // to sensor (tx/rx pins)

#define SEND                 // send messages via NB-IoT
#define M8M                  // if using a R410M with an UBX M8M

/*********************************************
    Pin definitions
*********************************************/
#define gpsEnablePin    GPS_ENABLE
#define soundSensorPin  A1      // analog pin 1
#define VREF            3.3     // sound sensor uses 3.3V power, used for calculations

/*********************************************
    GNSS module communication
*********************************************/
#define GPS_ADR 0x42            // GNSS module register address for reading/writing data 
#define BUFFER_SIZE 256         // max size of received NMEA messages and UBX frames
uint8_t buffer[BUFFER_SIZE];    // buffer to store read data from GNSS receiver

/*********************************************
    GNSS NMEA message configuration
*********************************************/
#define GALILEO         // enable Galileo navigation (and GPS and GLONASS)
#define NMEA41          // use NMEA version 4.1 (required for Galileo)
#define RXMSFRBX        // output UBX RXM-SFRBX messages
// ONLY USE ONE OF THE MESSAGES BELOW AT ONCE
//#define GLL       // geographic position, currentLat, currentLon
#define GGA         // global positioning system fix data
//#define GSA       // gps dop and active satellites
//#define GSV       // gps satellites in view
//#define RMC       // recommended minimum specific gps/transit data
//#define VTG       // track made good and ground speed

/*********************************************
    Battery voltage parameters
*********************************************/
#define ADC_AREF    3.3f
#define BATVOLT_R1  4.7f
#define BATVOLT_R2  10.0f
#define BATVOLT_PIN BAT_VOLT

/*********************************************
    NB-IoT coinfiguration
 ********************************************/
#define CURRENT_APN             "lpwa.whatever.iot"                // operator access point name
#define CURRENT_OPERATOR        "24001" //AUTOMATIC_OPERATOR    // use automatic for not needing to configure for specific country, if not connecting change to operator id for current country
#define CURRENT_URAT            SODAQ_R4X_NBIOT_URAT            // for using nb-iot
#define CURRENT_MNO_PROFILE     MNOProfiles::STANDARD_EUROPE    // use STANDARD_EUROPE for newest firmware, older uses SWD_DEFAULT to connect correctly
#define CDP                     "XXX.XXX.XXX.XX"                 //TE connected device platform used
#define CDP_PORT                55050                           // port used at cdp for listening for incoming messages
#define SENSOR_BAUD             9600                            // air quality sensor uses slower baud rate
#define DEFAULT_BAUD            115200                          // baud rate used by R410M

static Sodaq_R4X r4x;                       // nb-iot module library object
static Sodaq_SARA_R4XX_OnOff saraR4xxOnOff; // on/off pin definition

static uint8_t lastResetCause;              // used to store the last reset cause

uint8_t  socketID;           // used to send udp-packets through nb-iot network
uint16_t localPort = 16666;  // port number for local socket used to send data
char     boardID[16];        // store the IMEI of the SIM, used for identifying the box

/*********************************************
    GNSS configuration binary representations
*********************************************/
byte enableGalileo[]        = {0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x08, 0x10, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x02, 0x04, 0x08, 0x00, 0x01, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x08, 0x0E, 0x00, 0x01, 0x00, 0x01, 0x01, 0x2E, 0x75};
byte changeNMEAVersion[]    = {0xB5, 0x62, 0x06, 0x17, 0x14, 0x00, 0x00, 0x41, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x75, 0x57};
byte disableGGA[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x00, 0x24};
byte disableGLL[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x2B};
byte disableGSA[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x02, 0x32};
byte disableGSV[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x39};
byte disableRMC[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x04, 0x40};
byte disableVTG[]           = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0xF0, 0x05, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x05, 0x47};
byte enableRXMSFRBX[]       = {0xB5, 0x62, 0x06, 0x01, 0x08, 0x00, 0x02, 0x13, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x25, 0x3E};
byte refreshRate[]          = {0xB5, 0x62, 0x06, 0x08, 0x06, 0x00, 0xE8, 0x03, 0x01, 0x00, 0x01, 0x00, 0x01, 0x39};

/*********************************************
    Variables
 ********************************************/
double currentLat   = 0.0;
double currentLon   = 0.0;
double oldLat       = 0.0;
double oldLon       = 0.0;
double timestamp    = 0.0;
double minimumDistanceMoved = 5.0;

int numberOfMeasurements     = 5;     // number of samples used for calculating an average
int stationaryCounter        = 0;     // used to detect if sensor is not moving
int failedReconnects         = 0;
int measurementDelayStandard = 1000;  // 1 second
int measurementDelayExtended = 10000; // 10 seconds
int measurementIntensity     = measurementDelayStandard;  

bool stationary             = false; // mobility flag
bool isConnected            = false;
bool hasLostConnection      = false;

/**********************************************
    Function prototypes
 *********************************************/
void    enableGPS();
void    sendGNSSConfig();
bool    nbiotConnect();
bool    getId();
size_t  readGPS();
size_t  readUbloxI2cStream();
double  getLat(String & data);
double  getLon(String & data);
double  getTimestamp(String & data);
double  getAltitude(String & data);
String  getField(String & data, int index);
void    getFix();
void    sendMessage(const char* payload);
void    sendStartupMessage();
String  readSensor();
void    extractValueFromSensorString(double *data, String outputString);
double  convertToUgPerM3(String gas, double ppb, double temp);
double  distanceBetween(double lat1, double lon1, double lat2, double lon2);
float   getSoundLevel();
uint16_t getBatteryVoltage();
void    ledOn(char colour);
static void printCpuResetCause(Stream& stream);

/**********************************************
    setup
    Run once at startup, defines pins and does
    prepatation for main functionality
 *********************************************/
void setup() {
    lastResetCause = PM->RCAUSE.reg;    // read cpu reset cause from register

    sodaq_wdt_disable(); // disable watchdog at setup stage
    
#ifdef DEBUG    
    while (!DEBUG);   // wait until serial connection is established, only for debugging    
    DEBUG.begin(DEFAULT_BAUD);
#endif    
    while ((!SENSOR) && (millis() < 5000)) {
        // wait for sensor for 5 seconds
    }
    SENSOR.begin(SENSOR_BAUD);
    
#ifdef DEBUG
    printCpuResetCause(DEBUG);
#endif

    enableGPS();    // enables the GNSS module

    Wire.begin();   // I2C initialization

    // configure pins
    pinMode(LED_GREEN,   OUTPUT);
    pinMode(LED_RED,     OUTPUT);
    pinMode(LED_BLUE,    OUTPUT);
    
#ifdef M8M
#ifdef DEBUG
    DEBUG.println("Sending configuration to GNSS module");
#endif
    sendGNSSConfig();           // configure gnss module according to pre-defined settings
    sodaq_wdt_safe_delay(100);
#endif
    // enable and start nb-iot module
    UBLOX.begin(r4x.getDefaultBaudrate());
#ifdef DEBUG
    r4x.setDiag(DEBUG); // direct output to serial port for debugging
#endif
    r4x.init(&saraR4xxOnOff, UBLOX);

    // enable and restart watchdog timer
    sodaq_wdt_enable(WDT_PERIOD_8X); // 8 seconds
    sodaq_wdt_reset();
#ifdef DEBUG
    DEBUG.println(lastResetCause);
    DEBUG.println(getCpuResetCause());
#endif    
    nbiotConnect(); // try to connect to nb-iot network

    // retreive imei number of board, used for unique identification
    if (getId()) {
#ifdef DEBUG        
        DEBUG.print("Board ID is: " );
        for (int i = 0; i < sizeof(boardID); i++) {
            DEBUG.print(boardID[i]);
        }
        DEBUG.println();
#endif        
        sendStartupMessage();   // show that we are live
    }
    else {
#ifdef DEBUG
        DEBUG.println("Error: could not obtain IMEI of SIM-card");
#endif        
    }
}

/**********************************************
    loop
    The main functionality of the program. Runs
    indefinitely.
 *********************************************/
void loop() {
    sodaq_wdt_reset();  // reset wdt each loop to indicate normal operation
    
    getFix(); // wait for location
    
    String sensorOutput     = "";
    float soundLevel        = 0.0;
    double bufferedValues   = 0.0;
    double temp             = 0.0;
    float minSound          = 0.0;
    float maxSound          = 0.0;
    float medianSound       = 0.0;
    float avgSound          = 0.0;

    if (hasFix()) {
        // device has previously known location
        if (distanceBetween(currentLat, currentLon, oldLat, oldLon) < minimumDistanceMoved) {
            // device has moved less than the minimum defined distance since last fix            
            if (stationaryCounter == 4) {
                // device has been stationary past 4 fixes
                if (!stationary) {
                    // device is now stationary, change sampling rate
                    measurementIntensity = measurementDelayExtended;
                    stationary = true;
#ifdef DEBUG
                    DEBUG.println("Device is stationary, increasing time between readings.");
#endif
                }
                else {
#ifdef DEBUG
                    DEBUG.println("Device is still stationary.");
#endif
                }
            }
            else {
                // device has not moved since last fix, wait a bit more until declaring it stationary
                stationaryCounter++;
#ifdef DEBUG
                DEBUG.println("Device has NOT moved since last fix!");
#endif
            }
        }
        else {
            // device is roaming, restore default sampling rate
            measurementIntensity = measurementDelayStandard;
            stationaryCounter = 0;
            stationary = false;
#ifdef DEBUG            
            DEBUG.println("Device is roaming...");
#endif            
        }
    }
    
    int successfullMeasurements = 0; // only count positive values as valid
    // sample air quality sensor for values
    for (int i = 0; i < numberOfMeasurements; i++) {
        sodaq_wdt_reset();
        int airMeasurementPPM, tempMeasurement;
        char sensorData[40];
        if (SENSOR) {
            SENSOR.write('\r'); // trigger output of one measurement string
            sensorOutput = SENSOR.readStringUntil('\n');                                // capture result
            strncpy(sensorData, sensorOutput.c_str(), 40);                              // store as char array
            sscanf(sensorData, "%*d, %d, %d,", &airMeasurementPPM, &tempMeasurement);   // extract parameters
            
            double airMeasurement = convertToUgPerM3('n', (double)airMeasurementPPM, (double)tempMeasurement);
            // values of 0 or less cant be determined since they are relative to a "clean" environment without a set value
            if(airMeasurement <= 0.0) {
                // just jump to next iteration, decreasing i here can result in endless loop
                continue;
            }
            successfullMeasurements++;
#ifdef DEBUG        
            DEBUG.println(airMeasurement);
#endif
            bufferedValues += airMeasurement;
            temp += tempMeasurement;
            sodaq_wdt_safe_delay(measurementIntensity);
        }
    }

    double average = (successfullMeasurements > 0) ? (bufferedValues / successfullMeasurements) : 0.0;
    double avgTemp = (successfullMeasurements > 0) ? (temp / successfullMeasurements) : (temp / numberOfMeasurements);
    
#ifdef DEBUG
    DEBUG.println(String("Current NO2 concentration: ") + average + String("ug/m3"));
    DEBUG.println(String("Temperature: ") + avgTemp);
#endif
    // sample sound sensor for values
    double soundLvls[numberOfMeasurements];
    for (int i = 0; i < numberOfMeasurements; i++) {
        double sndLvl = getSoundLevel();
#ifdef DEBUG
        DEBUG.println(sndLvl);
#endif
        soundLvls[i] = sndLvl;
        sodaq_wdt_safe_delay(125);
    }
    double len  = sizeof(soundLvls)/sizeof(soundLvls[0]);
    minSound    = getMin(soundLvls, len);
    maxSound    = getMax(soundLvls, len);
    avgSound    = getAverage(soundLvls, len);
    medianSound = getMedian(soundLvls, len);
#ifdef DEBUG    
    DEBUG.println(String("Min sound level: ") + minSound);
    DEBUG.println(String("Max sound level: ") + maxSound);
    DEBUG.println(String("Average sound level: ") + avgSound);
    //DEBUG.println(averageSound);
    DEBUG.println(String("Median sound level: ") + medianSound);

#endif
    uint16_t batLevel = getBatteryVoltage();

#ifdef SEND
    sodaq_wdt_reset();
    // only send message if it actually contains something
    if (average >= 0.0) {
        // ugly measurement string
        String message = String("air") + String(",") + String(average) + String(",") + String(currentLat, 7)
                         + String(",") + String(currentLon, 7) + String(",") + String(timestamp) + String(",") + String(boardID)
                         + String(",") + String(batLevel);
        sendMessage(message.c_str());
    }
    sodaq_wdt_safe_delay(2000); // wait between sending messages

    if (avgSound > 0.0) {
        String message = String("sound") + String(",") + String(minSound, 1) + String(",") + String(maxSound, 1) +
                         String(",") + String(avgSound, 1) + String(",") + String(medianSound, 1) + String(",") + String(currentLat, 7)
                         + String(",") + String(currentLon, 7) + String(",") + String(timestamp) + String(",") + String(boardID)
                         + String(",") + String(batLevel);
        sendMessage(message.c_str());
    }
#endif
    // save old location in order to compare against new
    oldLat = currentLat;
    oldLon = currentLon;

    // TODO: add handling of unexpected program occurences here, send diagnostic messages to own datastream
    // 1. could not sample any postive air sensor values
    // 2. measurements are not realistic, need to be determined. Maybe force restart if this is the case
}

/**********************************************
    nbiotConnect
    Attempts to connect to the nb-iot network
    with the pre-defined parameters.
 *********************************************/
bool nbiotConnect() {
    // try to connect to nbiot endpoint and udp-server
#ifdef DEBUG    
    DEBUG.println("Trying to connect..");
#endif    
    ledOn('r');
    r4x.execCommand("AT+CPSMS=0");
    r4x.execCommand("AT+CEDRXS=0");
    r4x.execCommand("AT+UPSV=0");
    // try to disconnect from the network before trying to connect again
    if (hasLostConnection) {
        r4x.disconnect();
        sodaq_wdt_safe_delay(2000);
    }
    
    isConnected = r4x.connect(CURRENT_APN, CURRENT_URAT, CURRENT_MNO_PROFILE, CURRENT_OPERATOR, BAND_MASK_UNCHANGED, BAND_MASK_UNCHANGED);
#ifdef DEBUG
    DEBUG.println(isConnected ? "Connected successfully!" : "Network connection failed");
#endif   
    if (isConnected) {
        ledOn('g'); 
        // disable PSM and eDRX (enabled by default in newest software) after suggestion from manufacturer
        hasLostConnection = false;
        failedReconnects = 0;
    }
    else {
        failedReconnects++;
    }

    if (failedReconnects == 4) {
        // force a wdt timeout and reset microcontroller
        while(1);
    }
    return isConnected;
}

/**********************************************
    sendMessage
    Sends a message to the defined endpoint using
    its local socket and the nb-iot network.
 *********************************************/
void sendMessage(const char* payload) {
    // check that we are still connected
    if (r4x.isConnected()) {
        // socket is not closed, try to close it again
        if (!r4x.socketIsClosed(socketID)) {
            if(!r4x.socketClose(socketID)) {
#ifdef DEBUG                
                DEBUG.println("Error: Failed to close socket");
#endif                
                return;
            }
        }
        // create a socket on a local port
        socketID = r4x.socketCreate(localPort, UDP);
        if (socketID >= 7 || socketID < 0) {  
#ifdef DEBUG
            DEBUG.println("Failed to create socket");
#endif
        }
        sodaq_wdt_safe_delay(200);
        // check if the number of sent bits matches the payload
        if (strlen(payload) == r4x.socketSend(socketID, CDP, CDP_PORT, (const uint8_t*)payload, strlen(payload))) {
#ifdef DEBUG                
            DEBUG.println("Message successufully sent!");
#endif
            // close socket after sending
            if(!r4x.socketClose(socketID)) {
#ifdef DEBUG                
                DEBUG.println("Error: Failed to close socket");
#endif                
            }
        }
        else {
#ifdef DEBUG                
            DEBUG.println("Error: Could not send message");
#endif 
        }
    }
    else {
#ifdef DEBUG
        DEBUG.println("Not connected to NB-IoT network");
#endif
        hasLostConnection = true;
        nbiotConnect();
    }
}

/**********************************************
    sendStartupMessage
    Used to send an initial message to the endpoint
    to show that the device is alive.
 *********************************************/
void sendStartupMessage() {
    String msg = String("Starting,") + String(boardID) + String(",") + String(getBatteryVoltage()) + String(",") + String(getCpuResetCause());
#ifdef DEBUG    
    DEBUG.println("Sending startup message");
#endif   
    sendMessage(msg.c_str());
}

bool hasFix() {
    return (oldLat != 0.0) && (oldLon != 0.0);
}

/*********************************************
    getFix
    Reads incoming NMEA sentences until a valid
    fix is obtained
 ********************************************/
void getFix() {
    bool hasFix = false;
    long heartbeatTimeout = 300000; // 5 minutes
    uint32_t lastHeartbeat = millis();
    uint32_t start = millis();
#ifdef DEBUG    
    DEBUG.println("Waiting for fix..");
#endif    
    if(r4x.isConnected()) {
        ledOn('p');
    }
    else {
        ledOn('y');
    }
    while (!hasFix) {
        // send heartbeats once in a while
        sodaq_wdt_reset(); // reset each loop to prevent wdt timeout
        if ((millis() - lastHeartbeat) > heartbeatTimeout) {
            sendHeartbeat();
            lastHeartbeat = millis();   // reset timer
        }
        
        memset(buffer, 0, BUFFER_SIZE); // reset buffer
        size_t count = readGPS();
        // if there is data to be processed
        if (count != 0) {
            String data = (char*) buffer;
            // look after global positioning system fix data (3D fix)
            // GPGGA for UBX 8M, GNGGA for UBX M8M
#ifdef M8M
            if (data.startsWith("$GNGGA")) {
#else
            if (data.startsWith("$GPGGA")) {
#endif
                currentLat = convertToCoord(getLat(data));
                currentLon = convertToCoord(getLon(data));
                // check if we have a valid coordinate
#ifdef GGA
                int type = getFixType(data);
                if (type != 0) {
                    timestamp = getTimestamp(data);
                    hasFix = true;
                    ledOn('b');
#ifdef DEBUG                    
                    DEBUG.println(String(" time to find fix: ") + (millis() - start) + String("ms"));
                    DEBUG.println(String(" fix type: ") + type);
                    DEBUG.println(String(" lat = ") + String(currentLat, 7));
                    DEBUG.println(String(" lon = ") + String(currentLon, 7));
                    DEBUG.println(String(" timestamp: ") + String(timestamp));
                    DEBUG.println(String(" altitude: ") + String(getAltitude(data)));
#endif                    
                }
#endif
            }
            else if (data.startsWith("$GNTXT")) {
                  
            }
            else {
                // TODO: add handling for raw Galileo frame
                //parseUBX_RXM_SFRBX();              
            }
        }
    }
}

/**********************************************
    enableGPS
    Enables the on-board GNSS module and makes
    it start receiving data
 *********************************************/
void enableGPS() {
    pinMode(gpsEnablePin, OUTPUT);
    digitalWrite(gpsEnablePin, HIGH);
}

/**********************************************
    sendGNSSConfig
    Writes config hexcode to the GNSS module
    thus enabling and disabling parameters and output
 *********************************************/
void sendGNSSConfig() {
#ifdef GALILEO
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&enableGalileo[0], sizeof(enableGalileo));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5); // short delay needed between transmissions
#endif
#ifdef NMEA41
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&changeNMEAVersion[0], sizeof(changeNMEAVersion));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef GGA
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef GLL
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGLL[0], sizeof(disableGLL));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef GSA
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSA[0], sizeof(disableGSA));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef GSV
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSV[0], sizeof(disableGSV));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef RMC
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableRMC[0], sizeof(disableRMC));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifndef VTG
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableVTG[0], sizeof(disableVTG));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
#ifdef RXMSFRBX
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&enableRXMSFRBX[0], sizeof(enableRXMSFRBX));
    Wire.endTransmission();

    sodaq_wdt_safe_delay(5);
#endif
}

bool getId() {
    return r4x.getIMEI(boardID, sizeof(boardID));
}

/*********************************************
    readGPS
    Returns the number of bits read by the
    function readUbloxI2cStream
 ********************************************/
size_t readGPS() {
    return readUbloxI2cStream();
}

/********************************************
    readUbloxI2cStream
    Reads from register of the GNSS module and
    saves the bits in a buffer
 *******************************************/
size_t readUbloxI2cStream() {
    uint16_t count = 0;
    Wire.beginTransmission(GPS_ADR);
    Wire.write((uint8_t)0xFD);
    Wire.endTransmission(false);
    Wire.requestFrom(GPS_ADR, 2);
    count = (uint16_t)(Wire.read() << 8) | Wire.read();
    count = (count > BUFFER_SIZE) ? BUFFER_SIZE : count;

    if (count) {
        for (size_t i = 0; i < (count - 1); i++) {
            Wire.requestFrom(GPS_ADR, 1, false);
            buffer[i] = Wire.read();
        }
        Wire.requestFrom(GPS_ADR, 1);
        buffer[count - 1] = Wire.read();
    }
    return count;
}

/********************************************
    getLat
    Helper function for better readability
 *******************************************/
double getLat(String & data) {
#ifdef GGA
    String res = getField(data, 2);
    return res != "" ? res.toDouble() : 0.0;
#endif
#ifdef GLL
    String res = getField(data, 1);
    return res != "" ? res.toDouble() : 0.0;
#endif
}

/********************************************
    getLon
    Helper function for better readability
 *******************************************/
double getLon(String & data) {
#ifdef GGA
    String res = getField(data, 4);
    return res != "" ? res.toDouble() : 0.0;
#endif
#ifdef GLL
    String res = getField(data, 3);
    return res != "" ? res.toDouble() : 0.0;
#endif
}

/********************************************
    getTimeStamp
    Helper function for better readability
 *******************************************/
double getTimestamp(String & data) {
#ifdef GGA
    String res = getField(data, 1);
    return res != "" ? res.toDouble() : 0.0;
#endif
#ifdef GLL
    String res = getField(data, 5);
    return res != "" ? res.toDouble() : 0.0;
#endif
}

/********************************************
    getFixType
    Helper function for better readability
 *******************************************/
#ifdef GGA
int getFixType(String & data) {
    // possible quiality values:
    // 0 = no fix, 1 = autonomous GNSS fix,
    // 2 = differential GNSS fix, 4 = RTK fixed,
    // 5 = RTK float, 6 = estimated/dead reckoning fix
    String res = getField(data, 6);
    return res != "" ? res.toInt() : 0;
}
#endif

/********************************************
    isValidFix
    Helper function for better readability
 *******************************************/
#ifdef GLL
bool isValidFix(String & data) {
    String res = getField(data, 6);
    return res == "V" ? true : false;
}
#endif

/********************************************
    getAltitude
    Helper function for better readability
 *******************************************/
#ifdef GGA
double getAltitude(String & data) {
    String res = getField(data, 9);
    return res != "" ? res.toDouble() : 0.0;
}
#endif
/********************************************
    getField
    Given a NMEA message, extract a field with
    the index given.
 *******************************************/
String getField(String & data, int index) {
    int found = 0;
    int strIndex[] = {0, -1};
    int maxIndex = data.length() - 1;

    for (int i = 0; i <= maxIndex && found <= index; i++) {
        if (data.charAt(i) == ',' || i == maxIndex) {
            found++;
            strIndex[0] = strIndex[1] + 1;
            strIndex[1] = (i == maxIndex) ? i + 1 : i;
        }
    }
    return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}

/********************************************
    convertToCoord
    NMEA sentences are outputted in the form
    degrees, minutes and fractions of minutes.
    This function converts it to a readable
    latitude or longitude
 *******************************************/
double convertToCoord(double data) {
    double degMin = data;
    double minimum = 0.0;
    double decDeg = 0.0;

    minimum = fmod((double)degMin, 100.0);

    degMin = (int)(degMin / 100);
    decDeg = degMin + (minimum / 60);

    return decDeg;
}

void parseUBX_RXM_SFRBX() {
    byte *p = buffer + 6; // payload start
    uint8_t gnssId, svId, numWords;
    uint32_t w1, w2, w3, w4, w5, w6, w7, w8;
    // U1 = unsigned char
    // U4 = unsigned long
    
    // offset   format  name        description
    // 0        U1      gnssId      GNSS identifier
    // 1        U1      svId        satellite identifier
    // 4        U1      numWords    number of data words, up to 10
    // 8 + 4*N  U4      dwrd        the data words (N = numWords)
    
    gnssId = *((uint8_t *) &p[0x00]);
    
    // only care about frames from Galileo
    if (gnssId == 2) {
        svId        = *((uint8_t *)  &p[0x01]);
        numWords    = *((uint8_t *)  &p[0x04]);  
        w1          = *((uint32_t *) &p[0x08]);
        w2          = *((uint32_t *) &p[0x0C]);
        w3          = *((uint32_t *) &p[0x10]);
        w4          = *((uint32_t *) &p[0x14]);
        w5          = *((uint32_t *) &p[0x18]);
        w6          = *((uint32_t *) &p[0x1C]);
        w7          = *((uint32_t *) &p[0x20]);    
        w8          = *((uint32_t *) &p[0x24]);
#ifdef DEBUG        
        DEBUG.println("RXM-SFRBX "); 
        DEBUG.print("GNSS ID: ");
        DEBUG.println(gnssId); 
        DEBUG.print("Satellite ID: ");
        DEBUG.println(svId); 
        DEBUG.print("NumWords: ");
        DEBUG.println(numWords);
#endif        
        if (numWords <= 10) {
#ifdef DEBUG            
            DEBUG.println("Data words: ");
            DEBUG.print("DW1: ");
            DEBUG.println(w1, BIN);
            DEBUG.print("DW2: ");
            DEBUG.println(w2, BIN);
            DEBUG.print("DW3: ");
            DEBUG.println(w3, BIN);
            DEBUG.print("DW4: ");
            DEBUG.println(w4, BIN);
            DEBUG.print("DW5: ");
            DEBUG.println(w5, BIN);
            DEBUG.print("DW6: ");
            DEBUG.println(w6, BIN);
            DEBUG.print("DW7: ");
            DEBUG.println(w7, BIN);
            DEBUG.print("DW8: ");
            DEBUG.println(w8, BIN);
#endif            
        }
#ifdef DEBUG        
        DEBUG.println("-----------------------------------------");
#endif        
    }  
}

/**********************************************
    sendHeartbeat
    Used to send heartbeat messages to the UDP
    server in order to tell the system we're
    still alive. Can explain connectivity problems.
 *********************************************/
void sendHeartbeat() {
    uint16_t batLevel = getBatteryVoltage();
    String msg = String("heartbeat,") + String(boardID) + String(",") + String(batLevel);
#ifdef DEBUG    
    DEBUG.println("Sending heartbeat");
#endif    
    sendMessage(msg.c_str());
}

/**********************************************
    convertToUgPerM3
    Takes the value of pollutant measured in ppb
    and converts it to micrograms per cubic meter.
 *********************************************/
double convertToUgPerM3(char gas, double ppb, double temp) {
    // µg/m3 = ((ppb) * (12.187) * (M)) / (273.15 + °C)
    double M;
    double val = 12.187;
    double absZero = 273.15;

    switch (gas) {
        case 'o':
            // O3
            M = 2.00;
            break;
        case 's':
            // SO2
            M = 2.62;
            break;
        case 'n':
            // NO2
            M = 1.88;
            break;
        case 'c':
            // CO
            M = 1.145;
            break;
        default:
            // Unknown
            M = 0.00;
            break;
    }
    return (ppb * val * M) / (absZero + temp);
}

/**********************************************
    distanceBetween
    Calculations follow the "haversine formula"
    which determines the great-circle distance
    between two points on a sphere given their
    latitude and longitude.
 *********************************************/
double distanceBetween(double lat1, double lon1, double lat2, double lon2) {
    int R = 6371; // earth radius in km
    double dLat = deg2rad(lat2 - lat1); // delta latutude
    double dLon = deg2rad(lon2 - lon1); // delta longitude
    double a = sin(dLat / 2) * sin(dLat / 2) * cos(deg2rad(lat1)) * cos(deg2rad(lat2)) * sin(dLon / 2) * sin(dLon / 2);
    double c = 2 * atan2(sqrt(a), sqrt(1 - a));
    double distance = R * c; // distance in km
#ifdef DEBUG    
    DEBUG.println(String("distance since last fix: ") + String(distance / 1000) + " m");
#endif    
    return distance / 1000; // result in meters
}

/**********************************************
    deg2rad
    Converts degrees to radians.
 *********************************************/
double deg2rad(double deg) {
    return deg * (PI / 180);
}

/**********************************************
    getSoundLevel
    Samples the sound level sensor and converts 
    them to dBA using the provided formula by 
    the manufacturer of the board.
 *********************************************/
float getSoundLevel() {
    float voltageValue = analogRead(soundSensorPin) / 1024.0 * VREF;
    float dbValue = voltageValue * 50.0;
    return dbValue;
}

/**********************************************
    ledOn
    Turns the on-board led on with the specified
    colour. Disables any previous setting. Pin
    naming does not make sense, ignore it.
 *********************************************/
void ledOn(char colour) {
    switch (colour) {
        case 'r':
            // red
            digitalWrite(LED_BLUE, HIGH);
            digitalWrite(LED_RED, LOW);
            digitalWrite(LED_GREEN, HIGH);
            break;
        case 'g':
            // green
            digitalWrite(LED_BLUE, HIGH);
            digitalWrite(LED_RED, HIGH);
            digitalWrite(LED_GREEN, LOW);
            break;
        case 'b':
            // blue
            digitalWrite(LED_BLUE, LOW);
            digitalWrite(LED_RED, HIGH);
            digitalWrite(LED_GREEN, HIGH);
            break;
        case 'y':
            // yellow
            digitalWrite(LED_BLUE, HIGH);
            digitalWrite(LED_RED, LOW);
            digitalWrite(LED_GREEN, LOW);
            break;
        case 'p':
            // pink
            digitalWrite(LED_BLUE, LOW);
            digitalWrite(LED_RED, LOW);
            digitalWrite(LED_GREEN, HIGH);
            break;
        default:
            // not valid, do nothing
            break;
    }
}

/**********************************************
    getBatteryVoltage
    Reads the value of the battery analog pin
    and converts it to a voltage given the formula
    provided by the manufacturer (SODAQ).
 *********************************************/
uint16_t getBatteryVoltage() {
    uint16_t voltage = (uint16_t)((ADC_AREF / 1.023) * (BATVOLT_R1 + BATVOLT_R2) / BATVOLT_R2 * (float)analogRead(BATVOLT_PIN));
    return voltage;
}

/**********************************************
    printCpuResetCause
    Reads the register value for the last reset
    cause and print it to the output stream.
 *********************************************/
static void printCpuResetCause(Stream& stream) {
    stream.print("CPU reset by");

    if (PM->RCAUSE.bit.SYST) {
        stream.print(" Software");
    }
    
    if ((PM->RCAUSE.reg & PM_RCAUSE_WDT) != 0) {
        stream.print(" Watchdog");
    }

    if (PM->RCAUSE.bit.EXT) {
        stream.print(" External");
    }

    if (PM->RCAUSE.bit.BOD33) {
        stream.print(" BOD33");
    }

    if (PM->RCAUSE.bit.BOD12) {
        stream.print(" BOD12");
    }

    if (PM->RCAUSE.bit.POR) {
        stream.print(" Power On Reset");
    }

    stream.print(" [");
    stream.print(PM->RCAUSE.reg);
    stream.println("]");
}

static int getCpuResetCause() {
    if (PM->RCAUSE.bit.POR) {
        return 0;
    }
    if (PM->RCAUSE.bit.BOD12) {
        return 1;
    }
    if (PM->RCAUSE.bit.BOD33) {
        return 2;
    }
    if (PM->RCAUSE.bit.EXT) {
        return 4;
    }
    if ((PM->RCAUSE.reg & PM_RCAUSE_WDT) != 0) {
        return 5;
    }
    if (PM->RCAUSE.bit.SYST) {
        return 6;
    }
}

void swap (double* a, double* b) {
    double t = *a;
    *a = *b;
    *b = t;
}

int partition(double arr[], int low, int high) {
    double pivot = arr[high];
    int i = (low - 1);

    for (int j = low; j <= high - 1; j++) {
        if (arr[j] < pivot) {
            i++;
            swap(&arr[i], &arr[high]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quickSort (double arr[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, low, high);
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

double getMedian (double values[], int n) {
    quickSort(values, 0, n - 1);
    
    if (n % 2 != 0) {
        return (double)values[(n+1)/2];
    }

    return (double)(values[(n-1)/2] + values[n/2])/2.0;
}

double getMax(double values[], int n) {
    double maxVal = values[0];
    for (int i = 0; i < n; i++) {
        if (values[i] > maxVal) {
            maxVal = values[i];
        }
    }
    return maxVal;
}

double getMin(double values[], int n) {
    double minVal = values[0]; // something large
    for (int i = 0; i  < n; i++) {
        if (values[i] < minVal) {
            minVal = values[i];
        }
    }
    return minVal;
}

double getAverage(double values[], int n) {
    double res = 0;
    for (int i = 0; i < n; i++) {
        res = res + pow(10, values[i] / 10);
    }
    return 10 * log10(res/n);
}
