#include <Wire.h>
#include <Sodaq_wdt.h>

#define gpsEnablePin GPS_ENABLE
#define GPS_ADR 0x42            // GNSS module register address for reading/writing data 
#define BUFFER_SIZE 1000         // max size of received NMEA messages and UBX frames
uint8_t buffer[BUFFER_SIZE];    // buffer to store read data from GNSS receiver

const byte gnnsConf1[] = {0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x00, 0x20, 0x07, 0x00, 0x07, 0x0C, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, 0x01, 0x02, 0x06, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x01, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x01, 0x06, 0x07, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x29, 0x0D};
const byte gnnsConf2[] = {0xB5, 0x62, 0x06, 0x3E, 0x00, 0x00, 0x44, 0xD2};
const byte gnnsConf3[] = {0xB5, 0x62, 0x06, 0x3E, 0x3C, 0x00, 0x00, 0x20, 0x20, 0x07, 0x00, 0x07, 0x0C, 0x00, 0x01, 0x00, 0x01, 0x01, 0x01, 0x01, 0x03, 0x00, 0x01, 0x00, 0x01, 0x01, 0x02, 0x06, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x03, 0x08, 0x10, 0x00, 0x00, 0x00, 0x01, 0x01, 0x04, 0x00, 0x08, 0x00, 0x00, 0x00, 0x01, 0x03, 0x05, 0x00, 0x03, 0x00, 0x00, 0x00, 0x01, 0x05, 0x06, 0x07, 0x0A, 0x00, 0x01, 0x00, 0x01, 0x01, 0x4F, 0xB3};

const byte navPosllh[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x02, 0x01, 0x0E, 0x47};
const byte navTimegal[] = {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x25, 0x01, 0x31, 0x8D};
const byte navSat[] =     {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x01, 0x35, 0x01, 0x41, 0xAD};
const byte rxmSfrbx[] =   {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0x02, 0x13, 0x01, 0x20, 0x6C};

// disable NMEA messages
const byte disableGGA[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x00, 0x00, 0xFA, 0x0F};
const byte disableGLL[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x01, 0x00, 0xFB, 0x11};
const byte disableGSA[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x02, 0x00, 0xFC, 0x13};
const byte disableGSV[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x03, 0x00, 0xFD, 0x15};
const byte disableRMC[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x04, 0x00, 0xFE, 0x17};
const byte disableVTG[] =  {0xB5, 0x62, 0x06, 0x01, 0x03, 0x00, 0xF0, 0x05, 0x00, 0xFF, 0x19};

void setup() {
    SerialUSB.print("setup");

    sodaq_wdt_safe_delay(3000);
    
    SerialUSB.begin(115200);

    enableGPS();
    Wire.begin();
    
    sodaq_wdt_safe_delay(200);
    
    sendGNSSConfig();

    sodaq_wdt_safe_delay(200);
}

void loop() {

    memset(buffer, 0, BUFFER_SIZE); // reset buffer
    size_t count = readGPS();
    if (count != 0) {
        //String data = (char*) buffer;
        for (int i = 0; i < sizeof(buffer); i++) {
            printHex(buffer[i]);
        }
        SerialUSB.println();
    }
}

void printHex(uint8_t num) {
    char hexChar[2];

    sprintf(hexChar, "%02X", num);
    SerialUSB.print(hexChar);
}

void enableGPS() {
    pinMode(gpsEnablePin, OUTPUT);
    digitalWrite(gpsEnablePin, HIGH);
}

void sendGNSSConfig() {
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf1[0], sizeof(gnnsConf1));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf2[0], sizeof(gnnsConf2));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&gnnsConf3[0], sizeof(gnnsConf3));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGLL[0], sizeof(disableGLL));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSA[0], sizeof(disableGSA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGSV[0], sizeof(disableGSV));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableRMC[0], sizeof(disableRMC));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
     
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableVTG[0], sizeof(disableVTG));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    // for some reason needs to be done twice..
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&disableGGA[0], sizeof(disableGGA));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navPosllh[0], sizeof(navPosllh));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(5); // short delay needed between transmissions
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navTimegal[0], sizeof(navTimegal));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
    
    Wire.beginTransmission(GPS_ADR);
    Wire.write(&navSat[0], sizeof(navSat));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);

    Wire.beginTransmission(GPS_ADR);
    Wire.write(&rxmSfrbx[0], sizeof(rxmSfrbx));
    Wire.endTransmission();
    sodaq_wdt_safe_delay(100);
}

size_t readGPS() {
    return readUbloxI2cStream();
}

size_t readUbloxI2cStream() {
    uint16_t count = 0;
    Wire.beginTransmission(GPS_ADR);
    Wire.write((uint8_t)0xFD);
    Wire.endTransmission(false);
    Wire.requestFrom(GPS_ADR, 2);
    count = (uint16_t)(Wire.read() << 8) | Wire.read();
    count = (count > BUFFER_SIZE) ? BUFFER_SIZE : count;

    if (count) {
        for (size_t i = 0; i < (count - 1); i++) {
            Wire.requestFrom(GPS_ADR, 1, false);
            buffer[i] = Wire.read();
        }
        Wire.requestFrom(GPS_ADR, 1);
        buffer[count - 1] = Wire.read();
    }
    return count;
}
