## Mobile Sensing Gateway (MSG) project repository
This is a collection of all arduino sketches made during the implementation and testing of the Mobile sensing gateway, here called SAAM (Sound And Air Measurements). The main sketch used is under **saam_v3** and it is what's run on the plattforms.

![MSG](https://bitbucket.org/cnetsvenska/mobilesensinggateway/raw/master/images/Box.png)

The Mobile Sensing Gateway (MSG) is a hardware platform for collecting position based environmental data using sensors. The MSG box is built from the following components:

* NB-IoT communication card: Sodaq Sara-R410m with ublox EVAM8M-chip GNSS 
* SIM-card: Telia standard trio micro-SIM card NB-IoT
* Enclosure box: ABS light grey enclosure 125*80*35cm
* Box inside: 3D-printed supports ca 20 gram
* Sound Level Meter: Gravity SEN0232 DFROBOT
* NO2 sensor: SPEC Sensors, LLC 968-043
* Battery (optional): Luxorparts Li-Po 3,7 V with JST-PH connector 1200 mAh

The PM10 version of the box has a additionally:

* PM10 sensor: nova PM sensor SDS011
* Ventilation (used by the sensor for measuring): 20cm PVC hose 6x1.5mm


This README contains a short description of each of the most important sketches and what they are used for. First the program of the mobile platforms are described and thereafter some sketches which are used for different applicaitons along with a getting started description.

## saam_v3

The program which is run on Sodaq Sara R410M boards. Main functionality is the following. First a connection is made to the NB-IoT network with the parameters found in the sketch. On a successfull connection it continues to attempt obtaining a position fix of the device. When a location is known, the air and sound sensors are polled for values which are compiled into message strings and sent over the NB-IoT network as UDP packages. This is then repeated by first determining a position and the polling the sensors etc.

In case of a disconnect from the NB-IoT network, the modem will attmept to reconnect. If it fails multiple times the watchdog timer is forced to trigger which causes a CPU reset and the program will restart (see https://github.com/SodaqMoja/Sodaq_wdt).
Since location is the main focus of each measurement, no values will be sent if no new position is determined. As a feature, heartbeats are implemented to be sent each 5 minutes of no successfull position fix. These can be seen in the respecitve datastream of each box and in the logs of the UDP server.

If there is a problem connecting to the NB-IoT network, this is usually fixed by changing either the MNO profile or the operator used. If AUTOMATIC_OPERATOR is not working, change it to the operator id of the current country. For Sweden, this is 24001 for Telia sim cards.

There are two different boards used which are out measuring. The difference is the GNSS chip used. Boards using the **UBX EVA 8M** are not configurable and should not attempt changing the settings of the chip, see comment in the sketch. If the board uses the **UBX EVA M8M** then it is possible to configure it to use the desired sattelite constellations as well as enabling and disabling NMEA and UBX messages received. This is described more in detail under the next section. The default configuration used or these boards is to use Galileo, GPS and GLONASS, changing the NMEA version to 4.1 and disabling all messages except GGA (Global positioning system fix data).

For future development, mainly obtaining raw Galileo frames to use in the GOEASY project there is one method implemented, but not tested much, which is made to parse a certain UBX message and extract the raw data words from it. This is implemented from a Matlab representation. For receiving and parsing, the **UBX-RXM-SFRBX** message needs to be enabled in the configuration step.
## gps_passthrough

This sketch is used to obtain the binary representations of the configuration messages used for the **UBX EVA M8M**. This requires the sketch to be uploaded to the board and U-Center to be downloaded (https://www.u-blox.com/en/product/u-center). In the program, connect the Sodaq Sara R410M but pressing the connect button or choose it from the list. Then press **F7** and if you see messages flowing in, press **F9** and disable them by right clicking the corresponding item in the list. In the Binary Console the disable messages should show which can be copied. Messages starting with **B5 62 06** are the messages and those starting with **B5 62 05** are their respective acknowledges.

This can be used to obtain all configuration messages, some of which can be found in saam_v3. Just follow the steps above and send the configurations wanted and copy the binary representation.

For changing the constellation used (GPS, Galileo, GLONASS, Beidu), press Ctrl+F9 and choose GNSS in the list. Select which constellations should be enabled and press Send to trigger the message. This can then be found in the Binary Console. Observe that there can be multiple binary messages as a result.

## r4x_serial_passthrough

This sketch is used for testing AT-commands and is useful for determining the software version of the platform. The AT-commands are written in the written to the board in the serial monitor of the Arduino IDE. Each command which will both write the command and response in the serial monitor.

## galileoRawDataPassthrough
The program configures the UBLOX EVA M8M chip (does not work with the 8M) to disable all NMEA messages and enable certain UBLOX messages containing raw Galileo data to be outputted to the serial output. This can be extended to enable more messages, see previous points for how to find the configuration messages in binary form.

## Getting started

In order to start developing software for the Sodaq Sara R410M, a few things are needed. Firstly, the Arduino IDE (https://www.arduino.cc/en/main/software) is used for uploading sketches to the board.
Since the R410M differs from a regular Arduino board, special board files and libraries are used in order to compile correctly. Follow this guide to get everything you need: https://learn.sodaq.com/getting_started/.
The libraries which should be added are the following: **Sodaq_R4X.h** and **Sodaq_wdt.h**. The **Sodaq_nbIOT.h** library is deprecated and should not be used.
Remember to always pick the correct board when uploading a sketch, since the wrong one will fail to compile and in rare cases might damage the hardware.

## Affiliation
![GOEASY](https://bitbucket.org/cnetsvenska/nb-iotudpserver/raw/master/images/GOEASY_HD_Logo2.png)
This work is supported by the European Commission through the [GOEASY H2020 PROJECT](https://goeasyproject.eu/) under grant agreement No 776261.
